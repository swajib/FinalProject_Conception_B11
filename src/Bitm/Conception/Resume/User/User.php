<?php

namespace Example\Bitm\Conception\Resume\User;

include_once ($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'Finalproject_Conception_B11' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php'); 
class Users {

    public $table = "users";
    public $is_admin;
    public $user_name;
    public $password;
    public $email;

    public function __construct($data = false) {
        //  $this->id = $data['id'];
        $this->is_admin = $data['is_admin'];
        $this->user_name = $data['user_name'];
        $this->password = $data['password'];
        $this->email = $data['email'];

        $this->open_connection();
    }

    public function open_connection() {
        $this->connection = mysql_connect("localhost", "root", "");
        if (!$this->connection) {
            die("Database connection failed: " . mysql_error());
        } else {
            $db_select = mysql_select_db("resume", $this->connection);
            if (!$db_select) {
                die("Database selection failed: " . mysql_error());
            }
        }
    }

    public function close_connection() {
        if (isset($this->connection)) {
            mysql_close($this->connection);
            unset($this->connection);
        }
    }

    public function store() {

        if ($this->password == $this->retype_password) {

            $query = "INSERT INTO " . $this->table . " (`is_admin`,`user_name`,`password`,`email`) VALUES ( '" . $this->is_admin . "','" . $this->user_name . "','" . $this->password . "','" . $this->email . "')";
            //Utility::prx($query);
            $result = mysql_query($query);
            if ($result) {
                $_message = "Your username is " . $this->user_name . " Wait for Admin Approved";
                Utility::message($_message);
                header('Location: user_index.php');
            } else {
                $_message = "There is an error while saving data. Please try again later.";
                Utility::message($_message);
            }
        } else {
            $_message = "Your information is not correct. Please try again.";
            Utility::message($_message);
            header("location: login.php");
        }
    }

    public function login($myusername, $mypassword) {

        $sql = "SELECT * FROM " . $this->table . " WHERE user_name='$myusername' and password='$mypassword'";
        $result = mysql_query($sql);
        $count = mysql_num_rows($result);
        if ($count == 1) {
            // Register $myusername, $mypassword and redirect to file "login_success.php"

            $_SESSION['username'] = $myusername;
            $_SESSION['password'] = $mypassword;

            $_message = "Welcome, " . $myusername . "";
            Utility::message($_message);
            header("location: index.php");
        } else {
            $_message = "Wrong Username or Password";
            Utility::message($_message);
            header("location: login.php");
        }
    }

    public function index() {

        $objs = array();

        $query = "SELECT * FROM " . $this->table . " ORDER BY id DESC";
//        Utility::prx($query);
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $objs[] = $row;
        }
        return $objs;
    }

    public function delete($id = NULL) {

        $query = "DELETE FROM " . $this->table . " WHERE `id` = '$id'";
//       Utility::prx($query);
        if (mysql_query($query)) {
            $message = "<h2> User is deleted successfully.</h2>";
            Utility::message($message);
            header('Location: index.php');
        } else {
            die('Query problem' . mysql_error());
        }
    }

}
