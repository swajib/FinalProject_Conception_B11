<?php

namespace Example\Bitm\Conception\Resume\Utility;

include_once ($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'Finalproject_Conception_B11' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php'); 

class Utility {
    
    public $message = "";

    static public function d($param=false){
        echo "<pre>";
        var_dump($param);
        echo "</pre>";
    }
    
    static public function dd($param=false){
        self::d($param);
        die();
    }
    
    static public function pr($param=false){
        echo '<pre>';
        print_r($param);
        
    }
    
    static public function prx($param=false){
        self::pr($param);
        exit();
    }
    
    static public function redirect($url){
        header("Location:".$url);
    }
    
    static public function message($message = null){
        if(is_null($message)){ // please give me message
            $_message = self::getMessage();
            return $_message;
        }else{ //please set this message
            self::setMessage($message);
        }
    }
    
    static private function getMessage(){
        
        $_message =  $_SESSION['message'];
        $_SESSION['message'] = "";
        return $_message;
    }
    
    static private function setMessage($message){
        $_SESSION['message'] = $message;
    }
}