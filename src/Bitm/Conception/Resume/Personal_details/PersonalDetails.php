<?php

namespace Example\Bitm\Conception\Resume\Personal_details ;
use Example\Bitm\Conception\Resume\Utility\Utility;
include_once ($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'Finalproject_Conception_B11' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php'); 

class PersonalDetails {
    public $id = "";
    public $name = "";
    public $f_name = "";
    public $m_name = "";
    public $b_date = "";
    public $gender = "";
    public $status = "";
    public $religion = "";
    public $nationality = "";
    public $nid = "";
    public $alt_email = "";
    public $hphone = "";
    public $mobile = "";
    public $ophone = "";
    public $location = "";
    public $add = array();
    public $p_add = array();
//    public $add_street = "";
//    public $vill = "";
//    public $thana = "";
//    public $post_off = "";
//    public $dist = "";
//    public $p_add_street = "";
//    public $p_vill = "";
//    public $p_thana = "";
//    public $p_post_off = "";
//    public $p_dist = "";
    public $delete_at = "";
 
    function __construct($data = FALSE) {
        
        $cont = mysql_connect("localhost", "root", "") or die("Database can not conected");
        $db_select = mysql_select_db("resume") or die("Database can not Selected");
        
        if(is_array($data) && array_key_exists('id', $data) && !empty($data['id']) ){
        $this->id = $data['id'];
       }
       $this->name = $data['name'];
       $this->f_name = $data['f_name'];
       $this->m_name =  $data['m_name'];
       $this->b_date = $data['b_date'];
       $this->gender= $data['gender'];
       $this->status = $data['status'];
       $this->religion = $data['religion'];
       $this->nationality = $data['nationality'];
       $this->nid = $data['nid'];
       $this->alt_email = $data['alt_email'];
       $this->hphone = $data['hphone'];
       $this->mobile = $data['mobile'];
       $this->ophone = $data['ophone'];
       $this->location = $data['location'];
       $this->add = $data['add'];
       $this->p_add = $data['p_add'];
//       $this->add_street = $data['add_street'];
//       $this->vill= $data['vill'];
//       $this->thana = $data['thana'];
//       $this->post_off= $data['post_off'];
//       $this->dist = $data['dist'];
//       $this->p_add_street = $data['p_add_street'];
//       $this->p_vill  = $data['p_vill'];
//       $this->p_thana = $data['p_thana'];
//       $this->p_post_off = $data['p_post_off'];
//       $this->p_dist =$data['p_dist'];
         
    }
    
    public function showlist(){
        $show = array();
        $query = "SELECT * FROM `personal_details` ORDER BY  id DESC";
        $result = mysql_query($query);
        while ($row = mysql_fetch_assoc($result)){
            $show[] = $row;
        }
        return $show;
    }

    
    public function store(){
        $address = implode(",", $this->add);
        $p_address = implode(",", $this->p_add);
    $query = "INSERT INTO `personal_details`(`name`, `father_name`, `mother_name`, `date_of_birth`, `gender`, `marital_status`, `nationality`, `national_id_no`, `religion`, `present_address`, `permanent_address`, `current_location`, `home_phone`, `mobile`, `office_phone`, `alternative_email`) "
            . "VALUES ('".$this->name."','".$this->f_name."','".$this->m_name."','".$this->b_date."','".$this->gender."','".$this->status."','".$this->nationality."','".$this->nid."','".$this->religion."','".$address."','".$p_address."','".$this->location."','".$this->hphone."','".$this->mobile."','".$this->ophone."','".$this->alt_email."')";
//    Utility::dd($query);
    $result = mysql_query($query); 
    if($result){
            utility::message("Your Personal data is successfully saved");
        }else{
            utility::message("Unable to store data");
        }
        utility::redirect('list.php');
    }
}    
