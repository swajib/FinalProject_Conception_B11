<?php
include_once '../start.php';

use Example\Bitm\Conception\Resume\Utility\Utility;
use Example\Bitm\Conception\Resume\Personal_details\PersonalDetails;

//Utility::dd($_POST);
$my_detaits = new PersonalDetails();
$showdata = $my_detaits->showlist();

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Create Personal Profile</title>
        <link href="../../../../resource/bootstrap/css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../../../../resource/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../../../resource/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../../../resource/css/datepicker.css" rel="stylesheet" type="text/css"/>
        <link href="../../../../resource/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../../../resource/css/linecons.css" rel="stylesheet" type="text/css"/>
        <link href="../../../../resource/css/normalize.css" rel="stylesheet" type="text/css"/>
        <link href="../../../../resource/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <main id="domain">
            <div class="container">
                <div>
                    <nav class="navbar navbar-inverse">
                        <div class="container-fluid">
                          <!-- Brand and toggle get grouped for better mobile display -->
                          <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                              <span class="sr-only">Toggle navigation</span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">Brand</a>
                          </div>
                            <ul class="nav navbar-nav navbar-right">
                                <li ><a href="create.php">Add Personal Details</a></li>
                                <li class="active"><a href="#">List</a></li>
                                <li><a href="#">Show Profile</a></li>
                                <li><a href="#">Next</a></li>
                                <li><a href="#"><i class="fa fa-user"></i>Logout</a></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </nav>
                </div>
                
            <div class="mytop">
                <h1>Users personal Details here</h1>
            </div>
                <div class="castom1">
                    <p class="bg-warning"></p>
                    <div class="col-md-12 bg-success">
                        <div class="col-md-6">
                            
                        </div>
                        <div class="col-md-6">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#"><i class="fa fa-file-pdf-o"></i>Pdf</a></li>
                                <li><a href="#"><i class="fa fa-file-excel-o"></i>Excel</a></li>
                            </ul>
                        </div>
                    </div>.
                    <div class="">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td>Sl No.</td>
                                    <td>Name</td>
                                    <td>Father Name</td>
                                    <td>Mother Name</td>
                                    <td>Birth Day</td>
                                    <td>Gender</td>
                                    <td>Email</td>
                                    <td>Phone Number</td>
                                    <td>Present Address</td>
                                    <td>Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $sl = 0;
                                foreach ($showdata as $value) {
                                $sl++;
                                ?>
                                <tr>
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $value['name']; ?></td>
                                    <td><?php echo $value['father_name']; ?></td>
                                    <td><?php echo $value['mother_name']; ?></td>
                                    <td><?php $date= $value['date_of_birth']; 
                                        $mydate = date("d-M-Y", strtotime($date));
                                        echo $mydate;
                                    ?></td>
                                    <td><?php echo $value['gender']; ?></td>
                                    <td><?php echo $value['alternative_email']; ?></td>
                                    <td><?php echo $value['mobile']; ?></td>
                                    <td><?php echo $value['present_address']; ?></td>
                                    <td>
                                        <a class="btn btn-primary" href="#">Edit</a>
                                        <a class="btn btn-warning" href="#">Trash</a>
                                    </td>
                                </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>    
        </main>    
     
        
        
        
        
        
        
        
        <script src="../../../../resource/js/myjs.js" type="text/javascript"></script>
        <script src="../../../../resource/js/bootstrap-datepicker.js" type="text/javascript"></script>
        <script src="../../../../resource/js/jquery.min.js" type="text/javascript"></script>
        <script src="../../../../resource/bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <script src="../../../../resource/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../../resource/bootstrap/js/npm.js" type="text/javascript"></script>
        
    </body>
</html>
