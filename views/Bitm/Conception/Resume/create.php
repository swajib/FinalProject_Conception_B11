<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Create Personal Profile</title>
        <link href="../../../../resource/bootstrap/css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../../../../resource/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../../../resource/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../../../resource/css/datepicker.css" rel="stylesheet" type="text/css"/>
        <link href="../../../../resource/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../../../resource/css/linecons.css" rel="stylesheet" type="text/css"/>
        <link href="../../../../resource/css/normalize.css" rel="stylesheet" type="text/css"/>
        <link href="../../../../resource/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <main id="domain">
            <div class="container">
                <div>
                    <nav class="navbar navbar-inverse">
                        <div class="container-fluid">
                          <!-- Brand and toggle get grouped for better mobile display -->
                          <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                              <span class="sr-only">Toggle navigation</span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">Brand</a>
                          </div>
                            <ul class="nav navbar-nav navbar-right">
                                <li class="active"><a href="#">Personal Details</a></li>
                                <li><a href="list.php">List</a></li>
                                <li><a href="#">Show Profile</a></li>
                                <li><a href="#">Next</a></li>
                                <li><a href="#"><i class="fa fa-user"></i>Logout</a></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </nav>
                </div>
                
            <div class="mytop">
                <h1>Please enter your personal Details here</h1>
            </div>
                <div class="castom">
                    <form action="store.php" method="POST">
                        <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="col-md-10">
                                <div class="col-md-12" style="margin-left: 15px;">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Name">
                                </div>
                                <div class="form-group">
                                    <label for="f_name">Father Name</label>
                                    <input type="text" name="f_name" class="form-control" id="f_name" placeholder="Father Name">
                                </div>
                                <div class="form-group">
                                    <label for="m_name">Mother Name</label>
                                    <input type="text" name="m_name" class="form-control" id="m_name" placeholder="Mother Name">
                                </div>
                                <div class="form-group">
                                    <label for="date">Birth Date</label>
                                    <input type="date" name="b_date" class="form-control" id="date" placeholder="YYYY-MM-DD">
                                </div>
                                <div class="form-group">
                                    <label for="gender">Gender</label>
                                    <div class="radio">
                                      <label>
                                          <input type="radio" name="gender" id="gender" value="Male" checked tabindex="6">
                                        Male
                                      </label>
                                    </div>
                                    <div class="radio">
                                      <label>
                                          <input type="radio" name="gender" id="gender" value="Female" tabindex="7">
                                        Female
                                      </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="status">Marital Status</label>
                                    <div class="radio">
                                      <label>
                                          <input type="radio" name="status" id="status" value="Married" checked tabindex="6">
                                        Married
                                      </label>
                                    </div>
                                    <div class="radio">
                                      <label>
                                          <input type="radio" name="status" id="status" value="Unmarried" tabindex="7">
                                        Unmarried
                                      </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="religion">Religion</label>
                                    <select name="religion" class="form-control" id="religion">
                                        <option value="">--Select one--</option>
                                        <option value="Islam">Islam</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Christian">Christian</option>
                                        <option value="Buddha">Buddha</option>
                                        <option value="Others">Others</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="national">Nationality</label>
                                    <input type="text" name="nationality" class="form-control" id="national" placeholder="Nationality">
                                </div>
                                <div class="form-group">
                                    <label for="nid">National Id Number </label>
                                    <input type="text" name="nid" class="form-control" id="nid" placeholder="National Id Number">
                                </div>
                                <div class="form-group">
                                    <label for="byemail">Alternative Email</label>
                                    <input type="email" class="form-control" name="alt_email" id="byemail" placeholder="Email">
                                </div>
                                </div>    
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="col-md-10">
                                <div class="col-md-12" style="margin-left: 15px;">
                                    <div class="form-group">
                                        <label for="hpone">Home Phone</label>
                                        <input type="text" name="hphone" class="form-control" id="hpone" placeholder="Home Phone">
                                    </div>
                                    <div class="form-group">
                                        <label for="mobile">Mobile</label>
                                        <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile Number">
                                    </div>
                                    <div class="form-group">
                                        <label for="ophone">Office Phone</label>
                                        <input type="text" class="form-control" name="ophone" id="ophone" placeholder="Office Phone">
                                    </div>

                                    <div class="form-group">
                                        <label for="location">Current Location </label>
                                        <input type="text" class="form-control" name="location" id="location" placeholder="Current Location">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group col-md-12">
                                        <label for="add">Present Address</label>
                                        <input type="text" class="form-control" name="add['street']" id="add" placeholder="Street Address">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="vill">Village</label>
                                            <input type="text" class="form-control" name="add['vill']" id="vill" placeholder="Village">
                                        </div>
                                        <div class="form-group">
                                            <label for="thana">Thana</label>
                                            <input type="text" name="add['thana']" class="form-control" id="thana" placeholder="Thana">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="post">Post Office</label>
                                            <input type="text" class="form-control" name="add['post_off']" id="post" placeholder="Post office">
                                        </div>
                                        <div class="form-group">
                                            <label for="dist">District</label>
                                            <input type="text" class="form-control" name="add['dist']" id="dist" placeholder="District">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <div class="form-group col-md-12">
                                        <label for="p_add">Permanent Address</label>
                                        <input type="text" class="form-control" name="p_add[]" id="p_add" placeholder="Street Address">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="vill">Village</label>
                                            <input type="text" class="form-control" name="p_add[]" id="vill" placeholder="Village">
                                        </div>
                                        <div class="form-group">
                                            <label for="thana">Thana</label>
                                            <input type="text" name="p_add[]" class="form-control" id="thana" placeholder="Thana">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="post">Post Office</label>
                                            <input type="text" class="form-control" name="p_add[]" id="post" placeholder="Post office">
                                        </div>
                                        <div class="form-group">
                                            <label for="dist">District</label>
                                            <input type="text" class="form-control" name="p_add[]" id="dist" placeholder="District">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div  style="margin-left: 80px;">
                        <button class="btn btn-success  " name="submit" value="save" type="submit">Save</button>
                        <button class="btn btn-primary  "  type="reset">Reset</button>    
                        </div>
                    </form>
                </div>
            </div>
        </div>    
        </main>    
     
        
        
        
        
        
        
        
        <script src="../../../../resource/js/myjs.js" type="text/javascript"></script>
        <script src="../../../../resource/js/bootstrap-datepicker.js" type="text/javascript"></script>
        <script src="../../../../resource/js/jquery.min.js" type="text/javascript"></script>
        <script src="../../../../resource/bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <script src="../../../../resource/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../../resource/bootstrap/js/npm.js" type="text/javascript"></script>
        
    </body>
</html>
